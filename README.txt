Welcome to MathWorlds!

So far, MathWorlds allows you to create a world defined by an implicit relation f(x,y,z,c)=0.

Just compile the game as you would with minetest and then choose 'MathWorlds' in the mapgen menu! Have fun!

Don't forget to add ''static_spawnpoint = 50,20,0'' to your minetest.conf file or else you will fall into INFINITY!!

Instruction to compile:

1. cd to master directory

2. cmake . -DRUN_IN_PLACE=TRUE && make -j2

3. Go to bin folder and run ./minetest (for more details and options https://github.com/minetest/minetest)

4. Add the minetest game for extra fun (https://github.com/minetest/minetest_game)


MathWorlds is a fork of Minetest 0.4.15 and is released under the same license.